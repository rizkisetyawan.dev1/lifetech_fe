import { Center, Group, Loader, Text } from "@mantine/core";

function Loading() {
  return (
    <Center h={300}>
      <Group>
        <Loader size="sm" />
        <Text>Loading ...</Text>
      </Group>
    </Center>
  );
}

export default Loading;
