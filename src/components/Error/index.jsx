import { Center, Group, Text } from "@mantine/core";
import { IconAlertOctagon } from "@tabler/icons-react";
import PropTypes from "prop-types";

function Error({ message }) {
  return (
    <Center h={300}>
      <Group>
        <IconAlertOctagon size={20} color="red" />
        <Text c="red">Error : {message}</Text>
      </Group>
    </Center>
  );
}

Error.propTypes = {
  message: PropTypes.string.isRequired,
};

export default Error;
