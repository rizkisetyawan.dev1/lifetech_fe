import { Button, Group, Text, TextInput, Textarea } from "@mantine/core";
import { useForm } from "@mantine/form";
import { modals } from "@mantine/modals";
import { notifications } from "@mantine/notifications";
import { IconDeviceFloppy } from "@tabler/icons-react";
import PropTypes from "prop-types";
import React from "react";
import { useMutation } from "react-query";
import fetchData from "../../../../../utils/fetchData";

function JobForm({ action, data, onSaveSuccess }) {
  const form = useForm({
    initialValues: {
      title: "",
      company: "",
      location: "",
      company_logo: "",
      how_to_apply: "",
      description: "",
      type: "",
    },
  });

  const { mutate, isLoading, isError, error } = useMutation(
    (values) =>
      fetchData(action === "Add" ? "jobs" : `jobs/${data.id}`, {
        method: action === "Add" ? "POST" : "PUT",
        body: values,
      }),
    {
      onSuccess: () => {
        form.reset();
        onSaveSuccess();
        modals.closeAll();
        notifications.show({
          color: "green",
          message: `Successfully ${action} Job`,
        });
      },
    }
  );

  const handleSubmit = (values) => {
    mutate(values);
  };

  const handleChangeInput = (event, name) => {
    form.setFieldValue(name, event.currentTarget.value);
  };

  React.useEffect(() => {
    if (action === "Edit") {
      form.setValues(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <form onSubmit={form.onSubmit(handleSubmit)}>
      <TextInput
        required
        label="Title"
        value={form.values.title}
        onChange={(event) => handleChangeInput(event, "title")}
      />
      <TextInput
        required
        label="Company"
        mt="xs"
        value={form.values.company}
        onChange={(event) => handleChangeInput(event, "company")}
      />
      <TextInput
        required
        label="Employment Type"
        mt="xs"
        value={form.values.type}
        onChange={(event) => handleChangeInput(event, "type")}
      />
      <TextInput
        required
        label="Location"
        mt="xs"
        value={form.values.location}
        onChange={(event) => handleChangeInput(event, "location")}
      />
      <TextInput
        label="Logo URL"
        mt="xs"
        value={form.values.company_logo}
        onChange={(event) => handleChangeInput(event, "company_logo")}
      />
      <Textarea
        mt="xs"
        label="How To Apply"
        required
        value={form.values.how_to_apply}
        onChange={(event) => handleChangeInput(event, "how_to_apply")}
        autosize
        minRows={2}
      />
      <Textarea
        mt="xs"
        label="Description"
        required
        value={form.values.description}
        onChange={(event) => handleChangeInput(event, "description")}
        autosize
        minRows={3}
      />

      <Group justify="flex-end" mt="md">
        <Button
          type="submit"
          loading={isLoading}
          leftSection={<IconDeviceFloppy />}
        >
          Save
        </Button>
      </Group>
      {isError && (
        <Text mt={4} ta="right" c="red" size="xs">
          Error: {error}
        </Text>
      )}
    </form>
  );
}

JobForm.propTypes = {
  action: PropTypes.string.isRequired,
  data: PropTypes.object,
  onSaveSuccess: PropTypes.func.isRequired,
};

export default JobForm;
