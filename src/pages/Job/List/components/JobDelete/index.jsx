import { Button, Group, Text } from "@mantine/core";
import { modals } from "@mantine/modals";
import { notifications } from "@mantine/notifications";
import PropTypes from "prop-types";
import { useMutation } from "react-query";
import fetchData from "../../../../../utils/fetchData";

function JobDelete({ data, onDeleteSuccess }) {
  const { mutate, isLoading, isError, error } = useMutation(
    () =>
      fetchData(`jobs/${data.id}`, {
        method: "DELETE",
      }),
    {
      onSuccess: () => {
        onDeleteSuccess();
        modals.closeAll();
        notifications.show({
          color: "green",
          message: `Successfully Delete Job`,
        });
      },
    }
  );

  const handleDelete = () => {
    mutate();
  };

  return (
    <>
      <Text size="sm">
        Are you sure you want to delete job
        <Text span fw="bold">
          {" "}
          {data.title}{" "}
        </Text>
        in
        <Text span fw="bold">
          {" "}
          {data.company}{" "}
        </Text>
        ? This action is destructive and you will have to contact support to
        restore your data.
      </Text>
      <Group justify="flex-end" mt="lg">
        <Button variant="outline">No don&apos;t delete it</Button>
        <Button color="red" onClick={handleDelete} loading={isLoading}>
          Delete Job
        </Button>
      </Group>
      {isError && (
        <Text mt={4} ta="center" c="red" size="xs">
          Error: {error}
        </Text>
      )}
    </>
  );
}

JobDelete.propTypes = {
  data: PropTypes.object,
  onDeleteSuccess: PropTypes.func.isRequired,
};

export default JobDelete;
