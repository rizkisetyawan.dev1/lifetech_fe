import {
  ActionIcon,
  Divider,
  Flex,
  Group,
  Image,
  Menu,
  Text,
  Tooltip,
} from "@mantine/core";
import { IconDotsVertical, IconPencil, IconTrash } from "@tabler/icons-react";
import moment from "moment";
import PropTypes from "prop-types";

function JobItem({ data, onSelectJob, onDeleteJob, onEditJob }) {
  return (
    <>
      <Divider my="md" />
      <Group justify="space-between">
        <Image
          src={data.company_logo}
          fallbackSrc="https://placehold.co/56x56?text=No%20Photo"
          w={56}
        />
        <Flex
          flex={1}
          direction="column"
          style={{ cursor: "pointer" }}
          onClick={onSelectJob}
        >
          <Text size="lg" c="blue" fw="bold">
            {data.title}
          </Text>
          <Text>{data.company}</Text>
          <Text c="dimmed">
            {data.location} -{" "}
            <Text size="sm" span c="green" fw="bolder">
              {data.type}
            </Text>
          </Text>
        </Flex>
        <Flex
          direction="column"
          align="flex-end"
          justify="space-between"
          mih={75}
        >
          <Menu shadow="md" width={100}>
            <Menu.Target>
              <Tooltip label="Action">
                <ActionIcon variant="transparent" aria-label="Settings">
                  <IconDotsVertical
                    style={{ width: "70%", height: "70%" }}
                    stroke={1.5}
                  />
                </ActionIcon>
              </Tooltip>
            </Menu.Target>

            <Menu.Dropdown>
              <Menu.Label>Actions</Menu.Label>
              <Menu.Item
                leftSection={<IconPencil size={14} />}
                onClick={onEditJob}
              >
                Edit
              </Menu.Item>
              <Menu.Item
                leftSection={<IconTrash size={14} />}
                onClick={onDeleteJob}
              >
                Delete
              </Menu.Item>
            </Menu.Dropdown>
          </Menu>
          <Text c="dimmed" size="sm">
            {moment(data.created_at).fromNow()}
          </Text>
        </Flex>
      </Group>
    </>
  );
}

JobItem.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    company: PropTypes.string.isRequired,
    company_logo: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    created_at: PropTypes.string.isRequired,
  }).isRequired,
  onSelectJob: PropTypes.func.isRequired,
  onDeleteJob: PropTypes.func.isRequired,
  onEditJob: PropTypes.func.isRequired,
};

export default JobItem;
