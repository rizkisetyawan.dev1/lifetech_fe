import {
  Box,
  Button,
  Card,
  Container,
  Divider,
  Grid,
  Image,
  Paper,
  Text,
  Title,
  TypographyStylesProvider,
} from "@mantine/core";
import { IconChevronLeft } from "@tabler/icons-react";
import moment from "moment";
import { useQuery } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import { Error, Loading } from "../../../components";
import fetchData from "../../../utils/fetchData";

function Detail() {
  const { jobId } = useParams();
  const navigate = useNavigate();
  const queryJob = useQuery(["job", jobId], () => fetchData(`jobs/${jobId}`));
  const detailJob = queryJob.data?.data;

  const handleBackToJobs = () => {
    navigate("/jobs");
  };

  return (
    <Container size="md">
      <Button
        variant="transparent"
        leftSection={<IconChevronLeft size={14} />}
        onClick={handleBackToJobs}
      >
        Back
      </Button>
      {queryJob.isLoading && <Loading />}
      {queryJob.isError && <Error message={queryJob.error} />}
      {queryJob.isSuccess && (
        <Paper shadow="sm" p="lg" mb="xl">
          <Text>
            {detailJob.type} / {detailJob.location}
          </Text>
          <Title order={2}>{detailJob.title}</Title>
          <Text c="dimmed" size="xs">
            {moment(detailJob.created_at).format("DD MMMM, YYYY HH:mm")}
          </Text>
          <Divider my="lg" />
          <Grid gutter="lg">
            <Grid.Col span={{ base: 12, md: 8 }}>
              <TypographyStylesProvider>
                <div
                  dangerouslySetInnerHTML={{
                    __html: detailJob.description,
                  }}
                />
              </TypographyStylesProvider>
            </Grid.Col>
            <Grid.Col span={{ base: 12, md: 4 }}>
              <Grid gutter="lg">
                <Grid.Col span={{ base: 6, md: 12 }}>
                  <Card
                    shadow="sm"
                    mb="lg"
                    padding="lg"
                    radius="md"
                    withBorder
                    bg="var(--mantine-color-gray-0)"
                  >
                    <Card.Section>
                      <Box p="sm">
                        <Text fw="bolder">{detailJob.company}</Text>
                      </Box>
                      <Divider />

                      <Image
                        src={detailJob.company_logo}
                        fallbackSrc="https://placehold.co/150x100?text=No%20Photo"
                        h={150}
                      />
                    </Card.Section>
                  </Card>
                </Grid.Col>
                <Grid.Col span={{ base: 6, md: 12 }}>
                  <Card
                    shadow="sm"
                    padding="lg"
                    radius="md"
                    withBorder
                    bg="var(--mantine-color-yellow-0)"
                  >
                    <Card.Section>
                      <Box p="sm">
                        <Text fw="bolder">How To Apply</Text>
                      </Box>
                      <Divider />
                      <Box p="sm">
                        <TypographyStylesProvider>
                          <div
                            dangerouslySetInnerHTML={{
                              __html: detailJob.how_to_apply,
                            }}
                          />
                        </TypographyStylesProvider>
                      </Box>
                    </Card.Section>
                  </Card>
                </Grid.Col>
              </Grid>
            </Grid.Col>
          </Grid>
        </Paper>
      )}
    </Container>
  );
}

export default Detail;
