import { Button, Container, Group, Table } from "@mantine/core";
import { saveAs } from "file-saver";

const elements = [
  { position: 6, mass: 12.011, symbol: "C", name: "Carbon" },
  { position: 7, mass: 14.007, symbol: "N", name: "Nitrogen" },
  { position: 39, mass: 88.906, symbol: "Y", name: "Yttrium" },
  { position: 56, mass: 137.33, symbol: "Ba", name: "Barium" },
  { position: 58, mass: 140.12, symbol: "Ce", name: "Cerium" },
];

function Xlsx() {
  const exportToXLS = async () => {
    try {
      const response = await fetch(
        `${import.meta.env.VITE_BASE_URL}/api/xlsx`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(elements),
        }
      );

      if (!response.ok) {
        throw new Error("Failed to convert data to XLS");
      }

      const blob = await response.blob();
      saveAs(blob, "data.xlsx");
    } catch (error) {
      console.error("Error:", error);
    }
  };
  const rows = elements.map((element) => (
    <Table.Tr key={element.name}>
      <Table.Td>{element.position}</Table.Td>
      <Table.Td>{element.name}</Table.Td>
      <Table.Td>{element.symbol}</Table.Td>
      <Table.Td>{element.mass}</Table.Td>
    </Table.Tr>
  ));

  return (
    <Container size="md" py="lg">
      <Table>
        <Table.Thead>
          <Table.Tr>
            <Table.Th>Element position</Table.Th>
            <Table.Th>Element name</Table.Th>
            <Table.Th>Symbol</Table.Th>
            <Table.Th>Atomic mass</Table.Th>
          </Table.Tr>
        </Table.Thead>
        <Table.Tbody>{rows}</Table.Tbody>
      </Table>
      <Group justify="flex-start" mt="lg">
        <Button color="green" onClick={exportToXLS}>
          Export
        </Button>
      </Group>
    </Container>
  );
}

export default Xlsx;
