import { ActionIcon, Button, Container, Flex, Tooltip } from "@mantine/core";
import { MantineReactTable, useMantineReactTable } from "mantine-react-table";
import { useMemo } from "react";
import { useQuery } from "react-query";
import fetchData from "../../utils/fetchData";
import moment from "moment";
import { IconEdit, IconTrash } from "@tabler/icons-react";
import { modals } from "@mantine/modals";
import UserForm from "./components/UserForm";
import UserDelete from "./components/UserDelete";

function User() {
  const columns = useMemo(
    () => [
      {
        accessorKey: "username",
        header: "Username",
      },
      {
        accessorKey: "email",
        header: "Email",
      },
      {
        accessorKey: "name",
        header: "Name",
      },
      {
        accessorKey: "country",
        header: "Country",
      },
      {
        accessorKey: "created_at",
        header: "Created Date",
        accessorFn: (row) => moment(row.created_at).format("DD MMMM, YYYY"),
      },
    ],
    []
  );

  const queryUsers = useQuery(["users"], () => fetchData("users"));

  const fetchedUsers = queryUsers?.data?.data ?? [];

  const onRefetchData = () => {
    queryUsers.refetch();
  };

  const openAddModal = () => {
    modals.open({
      title: "Add New User",
      children: <UserForm action="Add" onSaveSuccess={onRefetchData} />,
    });
  };

  const openEditModal = (data) => {
    modals.open({
      title: "Edit User",
      children: (
        <UserForm action="Edit" data={data} onSaveSuccess={onRefetchData} />
      ),
    });
  };

  const openDeleteModal = (data) =>
    modals.open({
      title: "Delete User",
      centered: true,
      children: <UserDelete data={data} onDeleteSuccess={onRefetchData} />,
    });

  const table = useMantineReactTable({
    columns,
    data: fetchedUsers,
    enableRowActions: true,
    enableColumnOrdering: true,
    enableGlobalFilter: false,
    state: {
      isLoaaing: queryUsers.isLoading,
      showSkeletons: queryUsers.isLoading,
      showLoadingOverlay: queryUsers.isFetching,
      showAlertBanner: queryUsers.isError,
    },
    renderRowActions: ({ row }) => (
      <Flex gap="md">
        <Tooltip label="Edit">
          <ActionIcon onClick={() => openEditModal(row.original)}>
            <IconEdit />
          </ActionIcon>
        </Tooltip>
        <Tooltip label="Delete">
          <ActionIcon color="red" onClick={() => openDeleteModal(row.original)}>
            <IconTrash />
          </ActionIcon>
        </Tooltip>
      </Flex>
    ),

    renderTopToolbarCustomActions: () => (
      <Button onClick={openAddModal}>Create New User</Button>
    ),

    mantineToolbarAlertBannerProps: queryUsers.isError
      ? {
          color: "red",
          children: "Error loading data",
        }
      : undefined,
  });

  return (
    <Container size="lg" my="lg">
      <MantineReactTable table={table} />
    </Container>
  );
}

export default User;
