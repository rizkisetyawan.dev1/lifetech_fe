import { Button, Group, Text, TextInput } from "@mantine/core";
import { useForm } from "@mantine/form";
import { modals } from "@mantine/modals";
import { notifications } from "@mantine/notifications";
import { IconDeviceFloppy } from "@tabler/icons-react";
import PropTypes from "prop-types";
import React from "react";
import { useMutation } from "react-query";
import fetchData from "../../../../utils/fetchData";

function UserForm({ action, data, onSaveSuccess }) {
  const form = useForm({
    initialValues: {
      username: "",
      email: "",
      name: "",
      country: "",
    },
  });

  const { mutate, isLoading, isError, error } = useMutation(
    (values) =>
      fetchData(action === "Add" ? "users" : `users/${data.id}`, {
        method: action === "Add" ? "POST" : "PUT",
        body: values,
      }),
    {
      onSuccess: () => {
        form.reset();
        onSaveSuccess();
        modals.closeAll();
        notifications.show({
          color: "green",
          message: `Successfully ${action} User`,
        });
      },
    }
  );

  const handleSubmit = (values) => {
    mutate(values);
  };

  const handleChangeInput = (event, name) => {
    form.setFieldValue(name, event.currentTarget.value);
  };

  React.useEffect(() => {
    if (action === "Edit") {
      form.setValues(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <form onSubmit={form.onSubmit(handleSubmit)}>
      <TextInput
        required
        label="Username"
        value={form.values.username}
        onChange={(event) => handleChangeInput(event, "username")}
      />
      <TextInput
        required
        type="email"
        label="Email"
        mt="xs"
        value={form.values.email}
        onChange={(event) => handleChangeInput(event, "email")}
      />
      <TextInput
        required
        label="Name"
        mt="xs"
        value={form.values.name}
        onChange={(event) => handleChangeInput(event, "name")}
      />
      <TextInput
        required
        label="Country"
        mt="xs"
        value={form.values.country}
        onChange={(event) => handleChangeInput(event, "country")}
      />

      <Group justify="flex-end" mt="md">
        <Button
          type="submit"
          loading={isLoading}
          leftSection={<IconDeviceFloppy />}
        >
          Save
        </Button>
      </Group>
      {isError && (
        <Text mt={4} ta="right" c="red" size="xs">
          Error: {error}
        </Text>
      )}
    </form>
  );
}

UserForm.propTypes = {
  action: PropTypes.string.isRequired,
  data: PropTypes.object,
  onSaveSuccess: PropTypes.func.isRequired,
};

export default UserForm;
