async function fetchData(
  url,
  { method = "GET", query = {}, body = null } = {}
) {
  try {
    const baseUrl = import.meta.env.VITE_BASE_URL;
    const requestOptions = {
      method,
      headers: {
        "Content-Type": "application/json",
      },
    };

    if (method !== "GET" && body) {
      requestOptions.body = JSON.stringify(body);
    }

    const filteredQuery = Object.fromEntries(
      Object.entries(query).filter(
        ([, value]) => value !== "" && value !== null
      )
    );
    const queryParams = new URLSearchParams(filteredQuery);
    const queryString = queryParams.toString();

    const response = await fetch(
      `${baseUrl}/api/${url}${queryString && `?${queryString}`}`,
      requestOptions
    );

    const data = await response.json();
    if (!response.ok) {
      throw new Error(data.errors || "Error fetching data");
    }
    return data;
  } catch (err) {
    throw err.message;
  }
}

export default fetchData;
