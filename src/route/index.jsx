import { createBrowserRouter } from "react-router-dom";
import { Job, User, Xlsx } from "../pages";

const route = createBrowserRouter([
  {
    path: "/jobs",
    element: <Job.List />,
  },
  {
    path: "/jobs/:jobId",
    element: <Job.Detail />,
  },
  {
    path: "/users",
    element: <User />,
  },
  {
    path: "/xlsx",
    element: <Xlsx />,
  },
]);

export default route;
